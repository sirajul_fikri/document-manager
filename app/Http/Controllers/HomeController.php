<?php

namespace App\Http\Controllers;
use App\Document;
use App\Note;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $documents = Document::orderBy('created_at', 'DESC')->get();
        return view('home', compact('documents'));
    }

    public function new_upload()
    {
        return view('upload');    
    }

    public function save_upload(Request $request)
    {   
        $request->validate([
            'title' => 'required|string|max:150',
        ]);

        if ($request->hasFile('document')) 
        {
            $this->validate($request, [
                'document' => 'required',
            ]);
        }  

        $document_save                 = new Document;
        $document_save->title          = $request->input('title');   
        if ($request->hasFile('document')) 
        {
            $documentraw  = $request->document;
            $getFileExt   = $documentraw->getClientOriginalExtension();        
            $document_save->document_url  = $request->document->storeAs('document', time().'.'.$getFileExt);
        }
        $document_save->saveOrFail();

        return redirect('home');
    }

    public function list_note()
    {
        $notes = Note::orderBy('created_at', 'DESC')->get();
        return view('note', compact('notes'));
    }

    public function new_note()
    {
        return view('note_new');
    }

    public function save_note(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:150',
            'note' => 'required|string|max:1500',
        ]);

        $note_save          = new Note;
        $note_save->title   = $request->input('title');  
        $note_save->note    = $request->input('note');           
        $note_save->saveOrFail();

        return redirect('note');
    }
}
