@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Note</div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12" align="center">  
                            <a href="{{ route('home') }}" class="btn btn-primary">Home</a>                          
                            <a href="{{ url('upload') }}" class="btn btn-primary">Upload Document</a>
                            <a href="{{ url('note') }}" class="btn btn-primary">Note</a>
                        </div>
                    </div>
                    <br>
                    <a href="{{ url('note/new') }}" class="btn btn-success">New Note</a>
                    <br><br>
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table class="table ">
                                <tr>
                                    <th>Title</th>
                                    <th>Note</th>
                                </tr>
                                @foreach($notes as $note)
                                    <tr>
                                        <td>{{ $note->title }}</td>
                                        <td>{{ $note->note }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
