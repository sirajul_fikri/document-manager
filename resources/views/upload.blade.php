@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload Document</div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12" align="center">  
                            <a href="{{ route('home') }}" class="btn btn-primary">Home</a>                          
                            <a href="{{ url('upload') }}" class="btn btn-primary">Upload Document</a>
                            <a href="{{ url('note') }}" class="btn btn-primary">Note</a>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}

                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    {!! Form::label('title', 'Title*') !!}
                                    {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('title') }}</small>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <div class="form-group{{ $errors->has('document') ? ' has-error' : '' }}">
                                                    {!! Form::label('document', 'Document*') !!}
                                                    {!! Form::file('document', ['required' => 'required']) !!}
                                                    <small class="text-danger">{{ $errors->first('document') }}</small>
                                                </div>
                                            </div>                                    
                                            <div class="card-body">
                                                {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
                                                {!! Form::submit("Upload & Save", ['class' => 'btn btn-success']) !!}
                                            </div>
                                        </div>   
                                    </div>
                                </div>
                                                         
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
