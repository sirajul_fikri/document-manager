@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12" align="center">  
                            <a href="{{ route('home') }}" class="btn btn-primary">Home</a>                          
                            <a href="{{ url('upload') }}" class="btn btn-primary">Upload Document</a>
                            <a href="{{ url('note') }}" class="btn btn-primary">Note</a>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12" align="center">
                            <h4>Welcome to Document Manager</h4>
                        </div>
                        <br><br>
                        <div class="col-md-12 table-responsive">
                            <table class="table ">
                                <tr>
                                    <th>Title</th>
                                    <th>Action</th>
                                </tr>
                                @foreach($documents as $document)
                                    <tr>
                                        <td>{{ $document->title }}</td>
                                        <td><a class="btn btn-primary" href="{{ url("storage/".$document->document_url) }}" download>Download Document</a></td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
