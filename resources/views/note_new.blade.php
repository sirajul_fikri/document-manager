@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">New Note</div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12" align="center">  
                            <a href="{{ route('home') }}" class="btn btn-primary">Home</a>                          
                            <a href="{{ url('upload') }}" class="btn btn-primary">Upload Document</a>
                            <a href="{{ url('note') }}" class="btn btn-primary">Note</a>
                        </div>
                    </div>                    
                    <br><br>
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal']) !!}
                    
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            {!! Form::label('title', 'Title') !!}
                            {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            <small class="text-danger">{{ $errors->first('title') }}</small>
                        </div>
                        
                        <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                            {!! Form::label('note', 'Note') !!}
                            {!! Form::textarea('note', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            <small class="text-danger">{{ $errors->first('note') }}</small>
                        </div>
                       
                        {!! Form::submit("Save", ['class' => 'btn btn-success']) !!}
                        
                    
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
